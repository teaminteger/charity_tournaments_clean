var express = require('express');
var router = express.Router();

var database = require('../database');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', function(req, res, next){
  res.render('test');
});

router.get('/player/:id', function(req, res, next) {
  var summoner_name = "SummonerName";
  var summoner_id = 10;

  res.render('player', { player_id: req.params.id,
  						 summoner_id: summoner_id,
  						 summoner_name: summoner_name });
});

router.get('/organizations', function(req, res, next) {
  database.query('SELECT * FROM organizations', function(err, rows, fields) {
    if (err) throw err;
    if (rows.length > 0)
      res.render('organizations', { organizations_array: rows });
  });
});

router.get('/organization/:id', function(req, res, next) {
  res.render('organization', {title: 'Hi'});
});

router.get('/tournaments', function(req, res, next) {
  database.query('SELECT * FROM tournaments ORDER BY start_date DESC', function(err, rows, fields) {
    if (err) throw err;
    if (rows.length > 0)
      res.render('tournaments', { tournaments_array: rows });
    else
      res.send('No tournaments available');
  });
});

router.get('/tournament/:id', function(req, res, next) {
  // GROUP DONATION AMOUNT
  database.query('SELECT *,tournaments.id AS tournaments_id FROM tournaments LEFT JOIN payouts ON payouts.tournament_id = tournaments.id WHERE tournaments.id = '+req.params.id, function(err, rows, fields) {
    if (err) throw err;
    if (rows.length > 0)
    {
      var tournament_data = rows[0];
      database.query('SELECT *, matches.id AS match_internal_id, blue_team.name AS blue_team_name, red_team.name AS red_team_name FROM matches '+
                     'LEFT JOIN teams AS blue_team ON blue_team.id = blue_team_id '+
                     'LEFT JOIN teams AS red_team ON red_team.id = red_team_id '+
                     'WHERE tournament_id = ? ORDER BY round,matches.id ASC', [ req.params.id ], function(err, rows, fields) {
        var starting_teams = [];
        var team_list = [];
        var tournament_results = [];
        var current_round = -1;
        var round_results = [];
        var pending_matches = [];

        for (index in rows)
        {
          if (current_round === -1)
          {
            current_round = rows[index]["round"];
          }
          else if (current_round != rows[index]["round"])
          {
            tournament_results.push(round_results);
            current_round = rows[index]["round"];
            round_results = [];
          }

          if (rows[index]["round"] === 1)
          {
            var match = [];
            match.push(rows[index]["blue_team_name"] || 'No Team');
            match.push(rows[index]["red_team_name"] || 'No Team');
            starting_teams.push(match);
            team_list.push({name: rows[index]["blue_team_name"]});
            team_list.push({name: rows[index]["red_team_name"]});
          }
          
          var match_score = [];
          if (rows[index]["winning_team_id"] != 0)
          {
            if (rows[index]["blue_team_id"] === rows[index]["winning_team_id"])
            {
              match_score = [1,0];
            }
            else
            {
              match_score = [0,1];
            }
          }
          else if (rows[index]["blue_team_id"] != 0 &&
                   rows[index]["red_team_id"] != 0)
          {
            pending_matches.push({ match_id: rows[index]["match_internal_id"],
                                   tournament_code: rows[index]["tournament_code"],
                                   blue_team_id: rows[index]["blue_team_id"],
                                   red_team_id: rows[index]["red_team_id"],
                                   blue_team_name: rows[index]["blue_team_name"],
                                   red_team_name: rows[index]["red_team_name"] });
          }
          round_results.push(match_score);
        }

        if (round_results.length > 0)
        {
          tournament_results.push(round_results);
        }

        res.render('tournament', { tournament_data: tournament_data,
                                   starting_teams: JSON.stringify(starting_teams),
                                   team_list: team_list,
                                   tournament_results: JSON.stringify(tournament_results),
                                   pending_matches: pending_matches });
      });
    }
    else
      res.send('No tournament for that ID');
  });
});

router.get('/tournament_m', function(req, res, next) {
  res.render('tournament_matchmaking', { title: 'hi' });
});

router.get('/team/:id', function(req, res, next) {
  database.query('SELECT *, SUM(payouts.amount) AS payout_amount, teams.logo_path AS team_logo_path, teams.name AS team_name, orgs.name AS org_name FROM payouts RIGHT JOIN teams ON teams.id = payouts.team_id LEFT JOIN organizations AS orgs ON orgs.id = teams.org_id WHERE teams.id = ? GROUP BY payouts.team_id', [req.params.id], function(err, rows, fields) {
    if (err) throw err;
    var team_data = rows[0];
    database.query('SELECT * FROM teams_players LEFT JOIN players ON players.id = teams_players.player_id WHERE teams_players.team_id = ?', [req.params.id], function(err, rows, fields) {
      if (err) throw err;
      res.render('team', { team_data: team_data,
                           team_players: rows });
    });
  });
});

router.get('/teams', function(req, res, next) {
  database.query('SELECT *,teams.id AS team_id,SUM(amount) AS team_payout FROM payouts LEFT JOIN teams ON teams.id = payouts.team_id GROUP BY payouts.team_id', function(err, rows, fields) {
    if (err) throw err;
    if (rows.length > 0)
      res.render('teams', { team_array: rows });
  });
});

router.get('/tournament_browse', function(req, res, next) {
  res.render('tournament_browse');
});

router.post('/signup/:step', function(req, res, next){
  console.log(req.body);
  res.render('signup', { step: req.params.step,
                         summoner_name: req.body.summoner_name})
});

router.get('/signup/:step', function(req, res, next) {
    res.render('signup', { step: req.params.step });
});


router.get('/highscore', function(req, res, next) {
   res.render('highscore'); 
});

module.exports = router;
