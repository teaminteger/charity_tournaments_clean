var express = require('express');
var router = express.Router();

router.get('/player/:id', function(req, res, next) {
  res.send(req.params.id);
});

router.get('/team/:id', function(req, res, next) {
  res.send(req.params.id);
});

router.get('/organization/:id', function(req, res, next) {
  res.send(req.params.id);
});

router.get('/tournament/:id', function(req, res, next) {
  res.send(req.params.id);
});

router.get('/match/:id', function(req, res, next) {
  res.send(req.params.id);
});

module.exports = router;
