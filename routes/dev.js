var express = require('express');
var router = express.Router();
var request = require('request');

var database = require('../database');

router.get('/record_match/:tournament_id/:match_id/:winner_id', function(req, res, next) {
  database.query('UPDATE matches SET winning_team_id = ? WHERE id = ?', [ req.params.winner_id, req.params.match_id ], function(err, result) {
    if (err) throw err;
    
    res.redirect('/tournament/'+req.params.tournament_id);
  });
});

router.post('/create_tournament', function(req, res, next) {
  //var num_teams = Math.pow(2, Math.floor(Math.random()*3) + 1);
  var num_teams = 8;
  database.query('INSERT INTO tournaments (name, creator_team_id, num_teams, finished, region) VALUES (?, 1, ?, 0, "na")', [req.body.tournament_name, num_teams], function(err, result){
    if (err) throw err;
    
    var tournament_id = result.insertId;

    database.query('SELECT * FROM teams', function(err, rows, fields){
      if (err) throw err;

      var wait = 0;
      for (i=0; i<num_teams/2; i++)
      {
        var tournament_code = "";
        var post_data = {"teamSize" : 5,"spectatorType": "ALL","pickType" : "TOURNAMENT_DRAFT","mapType" : "SUMMONERS_RIFT"};
        wait++;
        request.post({url:'https://global.api.pvp.net/tournament/public/v1/code?tournamentId=1935',
                      headers: {
                        'Content-Type': 'application/json',
                        'X-Riot-Token': 'RIOT_API_KEY'
                      },
                      json: post_data}, function (err, httpResponse, body){
          if (err) throw err;

          tournament_code = body[0];

          var blue_team = rows[Math.floor(Math.random()*rows.length)].id;
          var red_team = rows[Math.floor(Math.random()*rows.length)].id;
          database.query('INSERT INTO matches (tournament_id, blue_team_id, red_team_id, round, winning_team_id, tournament_code) VALUES (?, ?, ?, 1, 0, ?)',
                         [tournament_id, blue_team, red_team, tournament_code]);

          wait--;
          if (wait == 0)
          {
            database.query('INSERT INTO payouts (team_id, tournament_id, amount) VALUES (?, ?, ?)',
                           [blue_team, tournament_id, req.body.donation], function (err, result){
                              if (err) throw err;
                              res.redirect('/tournament/'+tournament_id);
                           });
          }
        });
      }
    });
  });
});

module.exports = router;
