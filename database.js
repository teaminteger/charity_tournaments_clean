var mysql = require('mysql');

var pool = mysql.createPool(process.env.CLEARDB_DATABASE_URL);

module.exports = pool;