$(function() {
	var time = new Date();
	time.setMonth(10);
	time.setFullYear(2015);
	time.setDate(13);
	time.setHours(12);
	function updateTime() {
		var currentTime = new Date();
		var difference = new Date(time - currentTime);
		console.log(difference.getDate());
		$('#days').text(difference.getDate());
		$('#hours').text(difference.getHours());
		$('#minutes').text(difference.getMinutes());
		$('#seconds').text(difference.getSeconds());
	}
	
	setInterval(updateTime, 1000);
	
	updateTime(time);
});

